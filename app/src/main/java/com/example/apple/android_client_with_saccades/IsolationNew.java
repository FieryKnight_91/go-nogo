package com.example.apple.android_client_with_saccades;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class IsolationNew {


    double a[];
    List mins = new ArrayList();
    List maxs = new ArrayList();
    int i=1;
    int prev = 0;
    //----------------
    int min_saccadeDuration = 30;
    int loc_maxIndex = 0, loc_minIndex = 0, zeroCrossing = 0;
    private List saccades = new ArrayList();
    //----------------
    double direction = 0;



    void calculate_isolation()
    {
        for(int j=1, k = 0;j<a.length && (direction = a[j]-a[k]) == 0.0;j++, k++);

        if(direction == 0){
            //Array contains only same value.
            // maxs.add(-1024.0);
            // mins.add(-1024.0);
        }

        double maxVelocity = 0; //maintains the peakvelocity in each saccade
        if(direction < 0.0){
            while(i<a.length){
                for(;i<a.length && a[prev] >= a[i];i++,prev++)
                {
                    mins.add(-1024.0);maxs.add(-1024.0);
                    double difference = Math.abs(a[i] - a[prev]);
                    if(difference > maxVelocity){
                        maxVelocity = difference;
                    }
                }

                //mins.add(a[prev]);  /*-------hemanth---------
                loc_minIndex = prev;
                if (Math.abs(loc_minIndex-loc_maxIndex) >= min_saccadeDuration){
                    mins.add(a[prev]);
                    addNewSaccade(loc_maxIndex, loc_minIndex, maxVelocity);
                } else {
                    mins.add(-1024.0);
                }
                maxVelocity = 0;
                //-----------------------*/

                for(;i<a.length && a[prev] <= a[i];i++,prev++)
                {
                    maxs.add(-1024.0);mins.add(-1024.0);
                    double difference = Math.abs(a[i] - a[prev]);
                    if(difference > maxVelocity){
                        maxVelocity = difference;
                    }
                }

                //maxs.add(a[prev]);/*----------hemanth------
                loc_maxIndex = prev;
                if (Math.abs(loc_minIndex-loc_maxIndex) >= min_saccadeDuration){
                    maxs.add(a[prev]);
                    addNewSaccade(loc_minIndex, loc_maxIndex, maxVelocity);
                } else {
                    maxs.add(-1024.0);
                }
                maxVelocity = 0;
                //-----------------------*/

                i++;prev++;
            }
        }
        else{
            while(i<a.length){
                for(;i<a.length && a[prev] <= a[i];i++,prev++)
                {
                    maxs.add(-1024.0);mins.add(-1024.0);
                    double difference = Math.abs(a[i] - a[prev]);
                    if(difference > maxVelocity){
                        maxVelocity = difference;
                    }
                }

                //maxs.add(a[prev]);/*---------hemanth-------
                loc_maxIndex = prev;
                if (Math.abs(loc_minIndex-loc_maxIndex) >= min_saccadeDuration){
                    maxs.add(a[prev]);
                    addNewSaccade(loc_minIndex, loc_maxIndex, maxVelocity);
                } else {
                    maxs.add(-1024.0);
                }
                maxVelocity = 0;
                //-----------------------*/

                for(;i<a.length && a[prev] >= a[i];i++,prev++)
                {
                    mins.add(-1024.0);maxs.add(-1024.0);

                    double difference = Math.abs(a[i] - a[prev]);
                    if(difference > maxVelocity){
                        maxVelocity = difference;
                    }
                }

                //mins.add(a[prev]);/*---------hemanth-------
                loc_minIndex = prev;
                if (Math.abs(loc_minIndex-loc_maxIndex) >= min_saccadeDuration){
                    mins.add(a[prev]);
                    addNewSaccade(loc_maxIndex, loc_minIndex, maxVelocity);
                } else {
                    mins.add(-1024.0);
                }
                maxVelocity = 0;
                //-----------------------*/

                i++;prev++;
            }
        }
    }

    private void addNewSaccade(int begin, int peak, double velocity){
        SaccadeObject obj = new SaccadeObject();
        obj.setSaccade_beginning(begin);
        obj.setSaccade_peak(peak);
        obj.set_peakVelocity(velocity);

        saccades.add(obj);
    }




    double[] getMaxs()
    {
        double ar [] = new double[a.length];// double br[] = new double[maxs.size()];
        Iterator<Double> iterator = maxs.iterator();

        for(int i=0;i<maxs.size();i++) {
            if (iterator.hasNext()) {
                ar[i] = iterator.next();
            }
        }

        return ar;
    }
    double [] getMins()
    {
        double ar [] = new double[a.length];//double br[] = new double[maxs.size()];

        Iterator<Double> iterator = mins.iterator();
        for(int i=0;i<mins.size();i++) {
            if (iterator.hasNext()) {
                ar[i] = iterator.next();
            }
        }
        return ar;
    }

    int getLength()
    {
        return a.length;
    }

    void extract_Isolate(double series[])
    {
        a = new double[series.length];

        for (int i = 0; i < series.length; i++)
        {
            a[i] = series[i];
        }
    }


    //-----------------------------------------
    Object[] getSaccadesArray(){
        return saccades.toArray();
    }
}
