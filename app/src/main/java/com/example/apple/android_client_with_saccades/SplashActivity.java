package com.example.apple.android_client_with_saccades;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


public class SplashActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        //getActionBar().hide();

        setContentView(R.layout.activity_splash);

        RelativeLayout lyt = (RelativeLayout)findViewById(R.id.splash_layout);
        lyt.setBackgroundResource(R.drawable.splash_grad_bckgrnd);

        Thread displaySplash = new Thread(){
            public void run(){
                try {
                    sleep(4000);
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    finish();
                }
            }
        };
        displaySplash.start();
    }
}
