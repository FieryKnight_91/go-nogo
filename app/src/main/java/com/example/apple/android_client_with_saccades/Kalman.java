package com.example.apple.android_client_with_saccades;

public class Kalman
{
    Number NewSeries[];

        private double Q = 0.125;//0.0625;
        private double R = 1;
        private double P = 2, K,X;

        public double update(double measurement)
        {
            P = P + Q;
            K = P/(P+R);
            X = X + (measurement - X) * K;
            P = (1 - K)*P;
            return X;
        }
         void extract(Number series[])
        {
             NewSeries = new Number[series.length];

            for (int i = 0; i < series.length; i++)
            {
                 NewSeries[i] = series[i];
            }
            X = NewSeries[0].doubleValue();
        }

}
