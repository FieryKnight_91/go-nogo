package com.example.apple.android_client_with_saccades;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends ActionBarActivity
{

    TextView textResponse;
    EditText editTextAddress, editTextPort;
    Button buttonConnect, buttonClear,buttonDisconnect,buttonGameLaunch;
    String line = null;
    String data=null;
    BufferedReader in;
    boolean socketStatus = false;
    MyClientTask myClientTask;
    Socket socket = null;
    InputStream inputStream = null;
    DataOutputStream dataOutputStream = null;

    long lastTransmissionTime = 0;

    //boolean to check if connected to wifi or not
    boolean pressedConnect = false;

    boolean received_Game_Data = false;
    Bundle game_data_bundle = new Bundle();
    long start_ping_time = 0;

    //----------------
    long referenceTime = 0;

    EditText welcomeMsg;
    int readBufferPosition; byte[] readBuffer,readBuffer1;


    /*private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    *//**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param //activity
     *//*
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }*/



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextAddress = (EditText) findViewById(R.id.address);
        editTextPort = (EditText) findViewById(R.id.port);
        buttonConnect = (Button) findViewById(R.id.connect);
        buttonClear = (Button) findViewById(R.id.clear);
        buttonDisconnect = (Button)findViewById(R.id.disconnect);
        buttonGameLaunch = (Button)findViewById(R.id.game_launcer);
        textResponse = (TextView) findViewById(R.id.response);

        buttonConnect.setOnClickListener(buttonConnectOnClickListener);
        buttonDisconnect.setOnClickListener(buttonDisconnectOnClickListener);
        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textResponse.setText("");
            }
        });
        buttonGameLaunch.setOnClickListener(buttonGameLaunchOnClickListener);


        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "PWGNGD");
        if(!mediaStorageDir.exists())
        {
            mediaStorageDir.mkdir();
        }

        /*  To see max memory app is allowed before a hard out of memmory error occurs */
        Runtime rt = Runtime.getRuntime();
        long maxMemory = rt.maxMemory();
        Log.v("onCreate", "maxMemory:" + Long.toString(maxMemory));

        ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);  // How much memory my app should use given the dynamics of my device and OS
        int memoryClass = am.getMemoryClass();
        Log.v("onCreate", "memoryClass:" + Integer.toString(memoryClass));

    }

    View.OnClickListener buttonConnectOnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View arg0)
        {
            myClientTask = new MyClientTask(editTextAddress.getText().toString(),
                    Integer.parseInt(editTextPort.getText().toString()), "Hello ESP");
            myClientTask.execute();
            pressedConnect = true;
        }
    };

    View.OnClickListener buttonDisconnectOnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            if (!socketStatus)
                Toast.makeText(MainActivity.this, "SOCKET Closed!!", Toast.LENGTH_SHORT).show(); // Change message to "Socket Already closed"
            else {
                try {
                    onDisconnect();
                    if (myClientTask.isCancelled()) {
                        socket.close();
                      //  Toast.makeText(MainActivity.this, "Socket Closed!", Toast.LENGTH_SHORT).show(); Remove comments
                        socketStatus = false;
                    } else {
                        Toast.makeText(MainActivity.this, "Couldn't Disconnect! Please try again!", Toast.LENGTH_SHORT).show();
                        socketStatus = true;
                    }
                } catch (IOException e)
                {
                    e.printStackTrace();
                    //Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                }
            }

        }
    };

    View.OnClickListener buttonGameLaunchOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.saisrivatsa.attention_game");
            Intent launchIntent = new Intent();
            launchIntent.setAction("com.saisrivatsa.attention_game.action.LAUNCH_IT");
            if (start_ping_time == 0){
                start_ping_time = System.nanoTime();
            }
            if (launchIntent != null){
                launchIntent.putExtra("StartPing", start_ping_time);
                startActivityForResult(launchIntent, 2);
                //startActivity(launchIntent);
            } else {
                Toast.makeText(getApplicationContext(), "Please Install Attention Game..", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == 2) && data !=null){
            game_data_bundle = data.getBundleExtra("game_data");
            received_Game_Data = game_data_bundle.getBoolean("HasData");
            if (received_Game_Data){
                Toast.makeText(this, " Received Game Data", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, " No Game Data ", Toast.LENGTH_LONG).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onDisconnect()
    {
        start_ping_time = 0;
        myClientTask.cancel(true);
    }

    public void onClick(View v)
    {
        Intent i = new Intent(this,AnalysisActivity.class);

        i.putExtra("preload", !pressedConnect);

        if (received_Game_Data){
            i.putExtra("game_data", game_data_bundle);
        }
        startActivity(i);
    }

/*
public void onResendClick(View v)
    {
        Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                try{
                    dataOutputStream.writeBytes(" ");
                }catch(IOException e)
                {
                    e.printStackTrace();
                }

            }
        },200,1000);
    }
*/


    public class MyClientTask extends AsyncTask<Void, String, Void>
    {
        Context context = getApplicationContext();
        String dstAddress;
        int dstPort;
        String response,resp= null;
        String msgToServer,unimsg;
        long pinggTime = 0;

        DataInputStream dataInputStream = null;


        MyClientTask(String addr, int port, String msgTo)
        {
            dstAddress = addr;
            dstPort = port;
            msgToServer = msgTo;
        }

        @Override
        protected Void doInBackground(Void... arg0)
        {
            String line;
            readBufferPosition = 0;
            try {
                socket = new Socket(dstAddress, dstPort);
                socketStatus = true;
                dataOutputStream = new DataOutputStream(socket.getOutputStream());
                //dataInputStream = new DataInputStream(socket.getInputStream());
                inputStream = socket.getInputStream();


                if (msgToServer != null)
                {
                    dataOutputStream.writeBytes(msgToServer);
                    unimsg = " A B C D E F G H I J ";
                    dataOutputStream.writeBytes(unimsg);
                }


            } catch (UnknownHostException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                response = "UnknownHostException: " + e.toString();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                response = "IOException: " + e.toString();
            }

            while(socketStatus && !isCancelled())
            {
                try
                {
                    in = new BufferedReader(new InputStreamReader(inputStream));
                    if(pinggTime == 0){
                        pinggTime = System.nanoTime();
                    }

                    while((line = in.readLine())!= null)
                    {
                        resp+=line;
                        // publishProgress(resp);
                        publishProgress(resp);

                        //---------------------
                        resp+="," + "T"+(System.nanoTime() - pinggTime);
                        System.out.println(resp);
                        //---------------------

                        try{

                            /*File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "PWGNGD");
                            if(!mediaStorageDir.exists())
                            {
                                mediaStorageDir.mkdir();
                            }
                            try {
                                BufferedWriter out = new BufferedWriter(new FileWriter(mediaStorageDir+"/myfile_test.txt",false));
                                out.write(DataObject.getInstance().getSaccade_report());
                                out.flush();
                                out.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }*/
                            //fWriter.write("My file content \n");
                            // fWriter.write("-------Trying to write stream -------\n"); // response);

                            File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "PWGNGD");
                            String fileName = mediaStorageDir+"/myfile_live.txt";
                            FileWriter fWriter = new FileWriter(fileName);
                            //FileWriter fWriter = new FileWriter("/sdcard/myfile_test.txt");
                            fWriter.append(resp+"\n");
                            //fWriter.append(resp);
                            // fWriter.append(" ++ Read data successfully ++");
                            fWriter.flush();
                            fWriter.close();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }

                }catch(IOException e)
                {
                    e.printStackTrace();
                }
            }

            if (dataOutputStream != null) {
                try {
                    dataOutputStream.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            if (dataInputStream != null) {
                try {
                    dataInputStream.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            String msg = values[0];
            if(start_ping_time == 0){
                start_ping_time = System.nanoTime();
                pinggTime = start_ping_time;
                Timer t = new Timer();
                t.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        try{
                            dataOutputStream.writeBytes(" ");
                            System.out.println("Timer Schedule executed");
                        }catch(IOException e)
                        {
                            e.printStackTrace();
                        }
                    }
                },200,1000);

            }
            lastTransmissionTime = System.nanoTime();
            textResponse.append(msg+" ");

        }

        @Override
        protected void onPostExecute(Void result) {

            super.onPostExecute(result);
            // textResponse.setText(data);

            //start_ping_time = Long.parseLong(result);

           // textResponse.append(" + END");  //Remove comments
           // Toast.makeText(context, "Reached onPostExecute()", Toast.LENGTH_SHORT).show(); // -do-
        }
    }
   /* public void onBackPressed()
    {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Closing App ??")
                .setMessage("Are you sure you want to quit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", null).show();
    }*/
}