package com.example.apple.android_client_with_saccades;


public class FIR

{
    /**	The length of the filter (number of coefficients).
     */
    public static int		m_nLength;

    /**	The filter coefficients.
     */
    public static double[]		m_afCoefficients;

    /**	The buffer for past input values.
     This stores the input values needed for convolution.
     The buffer is used as a circular buffer.
     */
    public static double[]		m_afBuffer;

    /**	The index into m_afBuffer.
     Since m_afBuffer is used as a circular buffer,
     a buffer pointer is needed.
     */
    protected  static int		m_nBufferIndex;

    Number Series_FIR[];


    /**	Init a FIR filter with coefficients.

     // @param afCoefficients The array of filter coefficients.
     */
    public FIR()
    {
        final double [] coefs = {
                0.05771844259087,  0.06126781290652,  0.06438589946183,   0.0670385868422,
                0.06919673834954,  0.07083657503747,  0.07193998561174,  0.07249476279475,
                0.07249476279475,  0.07193998561174,  0.07083657503747,  0.06919673834954,
                0.0670385868422,  0.06438589946183,  0.06126781290652,  0.05771844259087
        };
        m_nLength = coefs.length;
        m_afCoefficients = new double[m_nLength];
        System.arraycopy(coefs, 0, m_afCoefficients, 0, m_nLength);
        m_afBuffer = new double[m_nLength];
        m_nBufferIndex = 0;

    }


    /**	Process an input sample and calculate an output sample.
     Call this method to use the filter.
     */
    public static double process(double fInput)
    {

        m_nBufferIndex = (m_nBufferIndex + 1) % m_nLength;
        m_afBuffer[m_nBufferIndex] = fInput;
        int	nBufferIndex = m_nBufferIndex;
        double	fOutput = 0.0;
        for (int i = 0; i < m_nLength; i++)
        {
            fOutput += m_afCoefficients[i] * m_afBuffer[nBufferIndex];
            nBufferIndex--;
            if (nBufferIndex < 0)
            {
                nBufferIndex += m_nLength;
            }
        }
        return fOutput;
    }


    /**	Returns the length of the filter.
     This returns the length of the filter
     (the number of coefficients). Note that this is not
     the same as the order of the filter. Commonly,
     the 'order' of a FIR filter is said to be the number
     of coefficients minus 1: Since a single coefficient
     is only an amplifier/attenuator, this is considered
     order zero.

     @return The length of the filter (the number of coefficients).
     */
    private int getLength()
    {
        return m_nLength;
    }


    /**	Get the frequency response of the filter at a specified frequency.
     This method calculates the frequency response of the filter
     for a specified frequency. Calling this method is allowed
     at any time, even while the filter is operating. It does not
     affect the operation of the filter.

     @param dOmega The frequency for which the frequency response
     should be calculated. Has to be given as omega values
     ([-PI .. +PI]).

     @return The calculated frequency response.
     */
    public double getFrequencyResponse(double dOmega)
    {
        double	dReal = 0.0;
        double	dImag = 0.0;
        for (int i = 0; i < getLength(); i++)
        {
            dReal += m_afCoefficients[i] * Math.cos(i * dOmega);
            dImag += m_afCoefficients[i] * Math.sin(i * dOmega);
        }
        double	dResult = Math.sqrt(dReal * dReal + dImag * dImag);
        return dResult;
    }


    /**	Get the phase response of the filter at a specified frequency.
     This method calculates the phase response of the filter
     for a specified frequency. Calling this method is allowed
     at any time, even while the filter is operating. It does not
     affect the operation of the filter.

     @param dOmega The frequency for which the phase response
     should be calculated. Has to be given as omega values
     ([-PI .. +PI]).

     @return The calculated phase response.
     */
    public double getPhaseResponse(double dOmega)
    {
        double	dReal = 0.0;
        double	dImag = 0.0;
        for (int i = 0; i < getLength(); i++)
        {
            dReal += m_afCoefficients[i] * Math.cos(i * dOmega);
            dImag += m_afCoefficients[i] * Math.sin(i * dOmega);
        }
        double	dResult = Math.atan2(dImag, dReal);
        return dResult;
    }

}