package com.example.apple.android_client_with_saccades;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class ReportActivity extends Activity implements AdapterView.OnItemSelectedListener {
    Button plotBttn;
    Spinner spinnerr;
    TextView tv, tv_sacc_info, tv_scr_mrt, tv_stim_tim;
    boolean preloadedData = true;
    int Max = 17,Min =1;
    public int fileNumSelected = 1; //removed volatile

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        final Bundle game_data_bundle = getIntent().getBundleExtra("game_data");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        tv = (TextView)findViewById(R.id.saccade_count);
        tv_sacc_info = (TextView)findViewById(R.id.saccade_info);
        tv_scr_mrt = (TextView)findViewById(R.id.mrt_scr_tv);
        tv_stim_tim = (TextView)findViewById(R.id.stim_tim_tv);

        tv_sacc_info.setMovementMethod(new ScrollingMovementMethod());
        tv_scr_mrt.setMovementMethod(new ScrollingMovementMethod());
        tv_stim_tim.setMovementMethod(new ScrollingMovementMethod());

        plotBttn = (Button)findViewById(R.id.plot_button1);
        //plotBttn.setEnabled(false);

        Button analysBtn = (Button)findViewById(R.id.button2);
        spinnerr = (Spinner)findViewById(R.id.spinner2);
        //analysBtn.setEnabled(preloadedData);

        //spinnerr.setEnabled(preloadedData);
        Integer[] items = new Integer[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18};
        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); //Added on 30 march 2017.. Disables Selector..
        spinnerr.setAdapter(adapter);                // Remove the 3 comments
        spinnerr.setOnItemSelectedListener(this);
        analysBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new EOGAnalysisAsync().execute(game_data_bundle);


            }
        });

        refreshSaccadeReport();
    }

    private void refreshSaccadeReport() {
        tv.setText("" + DataObject.getInstance().getSaccadeCount());
        tv_sacc_info.setText(DataObject.getInstance().getSaccade_report());
        tv_scr_mrt.setText(DataObject.getInstance().getmRT_SCR_report());
        tv_stim_tim.setText(DataObject.getInstance().getStimuli_times_report());
    }

    public void onClickPlot(View v){
        Intent intent = new Intent(this, PlotActivity.class);
        startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        fileNumSelected = (int)parent.getItemAtPosition(position);// Added a comment here to stop selection from list (Remove later) 30 March 2017

        System.out.println("new File Number is:: " +fileNumSelected);
        Toast.makeText(getApplicationContext(), "updated fileNumSelected is " + fileNumSelected, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
       // fileNumSelected = Min + (int)(Math.random() * ((Max - Min) + 1)); //No statement present

    }

    public class EOGAnalysisAsync extends AsyncTask<Bundle, Void, Void> {



        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            /*Toast.makeText(getApplicationContext(), "fileNumSelected is "+ fileNumSelected, Toast.LENGTH_LONG).show();
            tv.setEnabled(false);
            tv_sacc_info.setEnabled(false);
            tv_scr_mrt.setEnabled(false);
            tv_stim_tim.setEnabled(false);*/
        }

        @Override
        protected Void doInBackground(Bundle... params) {
            Bundle bundle = params[0];
           /* fileNumSelected = Min + (int)(Math.random() * ((Max - Min) + 1));
            System.out.println("new File Number is:: " +fileNumSelected);*/     //Done to remove file pre-selection from random
            new Analysis(bundle, preloadedData, fileNumSelected);
            return null;
            //return true;
        }


        @Override
        protected void onPostExecute(Void check) {
            super.onPostExecute(check);
            refreshSaccadeReport();
        }
    }
}
