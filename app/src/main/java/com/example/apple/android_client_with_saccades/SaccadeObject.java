package com.example.apple.android_client_with_saccades;


public class SaccadeObject extends Object {

    private final float Fs = 200, Fs_EOG = 256;
    private int saccade_beginning = 0;
    private int saccade_peak = 0;
    private long saccade_latency = 0;
    private boolean beginning_set = false;
    private boolean peak_set = false, EOG_256 = false;

    private double saccade_ratio = 0;           // Ratio of peakVelocity to saccade duration

    private double saccade_peak_velocity = 0;

    private long time = 0;

    public SaccadeObject() {
        super();
    }

    public SaccadeObject(boolean val){
        EOG_256 = val;
    }


    //---------------------------------------setter methods-----------------------------
    public void setSaccade_beginning(int saccade_beginning) {
        this.saccade_beginning = saccade_beginning;
        beginning_set = true;
        calculateSaccadeDuration();
    }

    public void setSaccade_peak(int saccade_peak) {
        this.saccade_peak = saccade_peak;
        peak_set = true;
        calculateSaccadeDuration();
    }

    public void set_peakVelocity(double val){
        saccade_peak_velocity = val;
        if (time != 0){
            this.saccade_ratio = saccade_peak_velocity/time;
        }
    }

    public void setSaccade_latency(long val){
        saccade_latency = val;
    }

    private void calculateSaccadeDuration(){
        if (beginning_set & peak_set){
            time = (long) (((float) (saccade_peak - saccade_beginning) / Fs) * 1000);
        }

    }


    //------------------------------getter methods--------------------------------------
    public long getSaccadeTime() {
        return time;
    }

   /* public long getSaccadeTime(boolean val){
        long res;
        if(val){
            res = (long) (((float) (time) / Fs_EOG) * 1000);     //when the EOG data acquired from desktop version.. which has sampling rate 256
        } else {
            res = (long) (((float) (time) / Fs) * 1000);
        }
        return res;
    }*/


    public double getSaccade_peak_velocity(){
        return saccade_peak_velocity;
    }

    /*public double getSaccade_peak_velocity(boolean val){
        if (val){
            return saccade_peak_velocity * Fs;
        } else{
            return saccade_peak_velocity * Fs_EOG;
        }
    }*/


    public int getSaccade_peak() {
        return saccade_peak;
    }

    public int getSaccade_beginning() {
        return saccade_beginning;
    }

    public long getSaccade_latency(){
        return saccade_latency;
    }

    public double getSaccade_ratio() {
        return saccade_ratio;
    }

}
