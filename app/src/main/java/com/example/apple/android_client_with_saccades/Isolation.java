package com.example.apple.android_client_with_saccades;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class Isolation {
    double a[];
    List mins = new ArrayList();
    List maxs = new ArrayList();
    int i=1;
    int prev = 0;
    //--------- Dash ----
    int loc_maxIndex = 0, loc_minIndex = 0, zeroCrossing = 0;
    private List saccades = new ArrayList();
    //-----------------------
    double direction = 0;



    void calculate_isolation()
    {
        for(int j=1, k = 0;j<a.length && (direction = a[j]-a[k]) == 0.0;j++, k++);

        if(direction == 0){
            //Array contains only same value.
            // maxs.add(-1024.0);
            // mins.add(-1024.0);
        }

        double maxVelocity = 0; //maintains the peakvelocity in each saccade
        if(direction < 0.0){
            while(i<a.length){
                for(;i<a.length && a[prev] >= a[i];i++,prev++)
                {
                    mins.add(-1024.0);maxs.add(-1024.0);
                    if (a[prev]>=0 && a[i]<=0){
                        zeroCrossing = prev;
                    }
                    if(zeroCrossing != 0){
                        double difference = Math.abs(a[i] - a[prev]);
                        if(difference > maxVelocity){
                            maxVelocity = difference;
                        }
                    }
                }

                //mins.add(a[prev]);  /*----------------
                loc_minIndex = prev;
                if (Math.abs(loc_minIndex-loc_maxIndex) >= 10){
                    mins.add(a[prev]);
                    if (zeroCrossing != 0 ) {
                        SaccadeObject obj = new SaccadeObject();
                        obj.setSaccade_beginning(zeroCrossing);
                        obj.setSaccade_peak(prev);
                        obj.set_peakVelocity(maxVelocity);
                        saccades.add(obj);
                        zeroCrossing = 0;
                    }
                } else {
                    mins.add(-1024.0);
                }
                maxVelocity = 0;
                //-----------------------*/

                for(;i<a.length && a[prev] <= a[i];i++,prev++)
                {
                    maxs.add(-1024.0);mins.add(-1024.0);
                    if (a[prev]<=0 && a[i]>=0){
                        zeroCrossing = prev;
                    }
                    if(zeroCrossing != 0){
                        double difference = Math.abs(a[i] - a[prev]);
                        if(difference > maxVelocity){
                            maxVelocity = difference;
                        }
                    }
                }

                //maxs.add(a[prev]);/*---------------
                loc_maxIndex = prev;
                if (Math.abs(loc_minIndex-loc_maxIndex) >= 10){
                    maxs.add(a[prev]);
                    if (zeroCrossing != 0){
                        SaccadeObject obj = new SaccadeObject();
                        obj.setSaccade_beginning(zeroCrossing);
                        obj.setSaccade_peak(prev);
                        obj.set_peakVelocity(maxVelocity);
                        saccades.add(obj);
                        zeroCrossing = 0;
                    }
                } else {
                    maxs.add(-1024.0);
                }
                maxVelocity = 0;
                //-----------------------*/

                i++;prev++;
            }
        }
        else{
            while(i<a.length){
                for(;i<a.length && a[prev] <= a[i];i++,prev++)
                {
                    maxs.add(-1024.0);mins.add(-1024.0);
                    if (a[prev]<=0 && a[i]>=0){
                        zeroCrossing = prev;
                    }
                    if(zeroCrossing != 0){
                        double difference = Math.abs(a[i] - a[prev]);
                        if(difference > maxVelocity){
                            maxVelocity = difference;
                        }
                    }
                }

                //maxs.add(a[prev]);/*--------------
                loc_maxIndex = prev;
                if (Math.abs(loc_minIndex-loc_maxIndex) >= 10){
                    maxs.add(a[prev]);
                    if (zeroCrossing != 0){
                        SaccadeObject obj = new SaccadeObject();
                        obj.setSaccade_beginning(zeroCrossing);
                        obj.setSaccade_peak(prev);
                        obj.set_peakVelocity(maxVelocity);
                        saccades.add(obj);
                        zeroCrossing = 0;
                    }
                } else {
                    maxs.add(-1024.0);
                }
                maxVelocity = 0;
                //-----------------------*/

                for(;i<a.length && a[prev] >= a[i];i++,prev++)
                {
                    mins.add(-1024.0);maxs.add(-1024.0);
                    if (a[prev]>=0 && a[i]<=0){
                        zeroCrossing = prev;
                    }
                    if(zeroCrossing != 0){
                        double difference = Math.abs(a[i] - a[prev]);
                        if(difference > maxVelocity){
                            maxVelocity = difference;
                        }
                    }
                }

                //mins.add(a[prev]);/*----------------
                loc_minIndex = prev;
                if (Math.abs(loc_minIndex-loc_maxIndex) >= 10){
                    mins.add(a[prev]);
                    if (zeroCrossing != 0 ) {
                        SaccadeObject obj = new SaccadeObject();
                        obj.setSaccade_beginning(zeroCrossing);
                        obj.setSaccade_peak(prev);
                        obj.set_peakVelocity(maxVelocity);
                        saccades.add(obj);
                        zeroCrossing = 0;
                    }
                } else {
                    mins.add(-1024.0);
                }
                maxVelocity = 0;
                //-----------------------*/

                i++;prev++;
            }
        }
    }
    double[] getMaxs()
    {
        double ar [] = new double[a.length];// double br[] = new double[maxs.size()];
        Iterator<Double> iterator = maxs.iterator();

        for(int i=0;i<maxs.size();i++) {
            if (iterator.hasNext()) {
                ar[i] = iterator.next();
            }
        }

        return ar;
    }
    double [] getMins()
    {
        double ar [] = new double[a.length];//double br[] = new double[maxs.size()];

        Iterator<Double> iterator = mins.iterator();
        for(int i=0;i<mins.size();i++) {
            if (iterator.hasNext()) {
                ar[i] = iterator.next();
            }
        }
        return ar;
    }

    int getLength()
    {
        return a.length;
    }

    void extract_Isolate(double series[])
    {
        a = new double[series.length];

        for (int i = 0; i < series.length; i++)
        {
            a[i] = series[i];
        }
    }


    //-----------------------------------------
    Object[] getSaccadesArray(){
        return saccades.toArray();
    }
}
