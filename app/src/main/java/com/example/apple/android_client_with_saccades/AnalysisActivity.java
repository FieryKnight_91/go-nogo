package com.example.apple.android_client_with_saccades;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


public class AnalysisActivity extends ActionBarActivity {

    Analysis a;
    /*Button plotBttn;
    Spinner spinnerr;
    TextView tv, tv_sacc_info, tv_scr_mrt, tv_stim_tim;*/
    Bundle game_data_bundle = new Bundle();
    boolean hasGameData = false;

    boolean preloadedData = false;

    public volatile int fileNumSel = 1;

    private boolean check_alert = true;

    TextView txtView_Res;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        game_data_bundle = getIntent().getBundleExtra("game_data");
        if (game_data_bundle !=null) {
            hasGameData = game_data_bundle.getBoolean("HasData");
        }
        preloadedData = getIntent().getBooleanExtra("preload", true);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analysis);
        //a = new Analysis();

        txtView_Res = (TextView) findViewById(R.id.textView_res);
        imageView = (ImageView) findViewById(R.id.imageView2);


        new AnalysisAsync().execute(game_data_bundle);


    }


    public void showReport(View view){
        Intent intent = new Intent(this, ReportActivity.class);
        intent.putExtra("game_data", game_data_bundle);
        startActivity(intent);
    }

    public void done(View view){
        super.onBackPressed();
    }


    public class AnalysisAsync extends AsyncTask<Bundle, Void, Boolean>{



        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            /*Toast.makeText(getApplicationContext(), "fileNumSelected is "+ fileNumSelected, Toast.LENGTH_LONG).show();
            tv.setEnabled(false);
            tv_sacc_info.setEnabled(false);
            tv_scr_mrt.setEnabled(false);
            tv_stim_tim.setEnabled(false);*/
        }

        @Override
        protected Boolean doInBackground(Bundle... params) {
            Bundle bundle = params[0];
            a = new Analysis(bundle, preloadedData);
            return a.getStatus();
            //return true;
        }


        @Override
        protected void onPostExecute(Boolean check) {
            super.onPostExecute(check);

            if(check)
            {
                txtView_Res.setText("You are Alert");
                imageView.setImageResource(R.drawable.go);
            }
            else
            {
                txtView_Res.setText("You are tired");
                imageView.setImageResource(R.drawable.nogo);
            }
        }
    }
}
