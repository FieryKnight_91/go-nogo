package com.example.apple.android_client_with_saccades;

/**
 *
 * Singleton class for Having only one 'instance' of the DataObject class.. and any class can call getInstance method to get that same object('instance')
 * Thus only one Dataobject object which maintains all the data and can be accessed fram any class by calling
 * DataObject.getInstance()
 */
public class DataObject {

    private Number series1Numbers[],series2Numbers[],series3Numbers[],series_Max_num[],series_Min_num[];
    private int saccadeCount = 0, isolated_ar_length = 0, rawArr_length;
    private double smallest,largest;
    private Object[] saccades;
    private long[] saccadeLtncyArr;
    private String saccade_report="---", mRT_SCR_report = "--", stimuli_times_report="--";


    private int eog_samp_rate = 200;

    //private constructor
    private static DataObject instance = new DataObject();

    private DataObject(){

    }


    //--------------getters----------------------------------------

    public static DataObject getInstance(){
        return instance;
    }


    public Number[] getSeries1Numbers() {
        return series1Numbers;
    }

    public Number[] getSeries2Numbers() {
        return series2Numbers;
    }

    public Number[] getSeries3Numbers() {
        return series3Numbers;
    }

    public Number[] getSeries_Max_num() {
        return series_Max_num;
    }

    public Number[] getSeries_Min_num() {
        return series_Min_num;
    }

    public int getSaccadeCount() {
        return saccadeCount;
    }

    public int getIsolated_ar_length() {
        return isolated_ar_length;
    }

    public int getRawArr_length() {
        return rawArr_length;
    }

    public double getSmallest() {
        return smallest;
    }

    public double getLargest() {
        return largest;
    }

    public Object[] getSaccadesArray() {
        return saccades;
    }

    public long[] getSaccadeLatencyArray(){
        return saccadeLtncyArr;
    }

    public String getSaccade_report() {
        return saccade_report;
    }

    public String getmRT_SCR_report(){
        return mRT_SCR_report;
    }

    public String getStimuli_times_report(){
        return stimuli_times_report;
    }

    public int getEog_samp_rate() {
        return eog_samp_rate;
    }

    //----------------------setters----------------------------------

    public void setEog_samp_rate(int eog_samp_rate) {
        this.
                eog_samp_rate = eog_samp_rate;
    }


    public void setSeries1Numbers(Number[] series1Numbers) {
        this.series1Numbers = series1Numbers;
        setRawArr_length(series1Numbers.length);
    }

    public void setSeries2Numbers(Number[] series2Numbers) {
        this.series2Numbers = series2Numbers;
    }

    public void setSeries3Numbers(Number[] series3Numbers) {
        this.series3Numbers = series3Numbers;
    }

    public void setSeries_Max_num(Number[] series_Max_num) {
        this.series_Max_num = series_Max_num;
    }

    public void setSeries_Min_num(Number[] series_Min_num) {
        this.series_Min_num = series_Min_num;
    }

    public void setSaccadeCount(int saccadeCount) {
        this.saccadeCount = saccadeCount;
    }

    public void setIsolated_ar_length(int isolated_ar_length) {
        this.isolated_ar_length = isolated_ar_length;
    }

    public void setRawArr_length(int rawArr_length) {
        this.rawArr_length = rawArr_length;
    }

    public void setSmallest(double smallest) {
        this.smallest = smallest;
    }

    public void setLargest(double largest) {
        this.largest = largest;
    }

    public void setSaccadesArray(Object[] saccades) {
        this.saccades = saccades;
        setSaccadeCount(this.saccades.length);
    }

    public void setSaccadeLtncyArr(long[] arr){
        this.saccadeLtncyArr = arr;
    }

    public void setSaccade_report(String saccade_report) {
        this.saccade_report = saccade_report;
    }

    public void setMRT_SCR_report(String val){
        this.mRT_SCR_report = val;
    }

    public void setStimuli_times_report(String val){
        this.stimuli_times_report = val;
    }

}
