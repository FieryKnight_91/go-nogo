package com.example.apple.android_client_with_saccades;

import android.os.Bundle;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.StringTokenizer;


public class Analysis
{

    public static String newline = System.getProperty("line.separator");
    private BufferedReader in;
    private StringTokenizer st;
    private String regex = "\\d+";
    private String time_regex = "[T]{1}\\d+";

    private ArrayList<Double> list = new ArrayList<Double>();

    private int saccadeCount = 0, ar_length = 0;
    private double result_FIR[], result[],smallest,largest;
    private double arr[];
    private Object[] saccades;

    private int hasBundle = 1;
    private long scr_arr[] = new long[7];
    private long mrt_arr[] = new long[7];
    private long stim_arr[];

    private long lastStimulus = 0;
    private long firstStimulus = 0, adjusted_firstTransmissionTime = 0;
    private int lastVRTIndex = 0, firstVRTIndex =0, considered_indices =0;
    private long avg_sacc_duration=0;
    private double avg_sacc_peakVelocity=0;
    private long avg_sacc_saccade_ltncy=0;
    private double avg_sacc_ratio = 0;

    BufferedWriter out = null;
    int fileNum =1;
    boolean hasFileNum = true, isAlertGame = false, isAlertEOG = false, alertResult;


    private DataObject dataObject;
    private int samplingRate = 200;
    private Bundle game_data_bundle;
    private boolean bundleHasData = false, preloaded;
    private Number series1Numbers[],series3Numbers[],series2Numbers[],series_Max_num[],series_Min_num[];

    public Analysis( Bundle val, boolean val2, int fNum) {
        game_data_bundle = val;
        preloaded = val2;         //remove the comments to load the testdata file
        this.fileNum = fNum;
        dataObject = DataObject.getInstance();
        samplingRate = dataObject.getEog_samp_rate();
        readFile();
        calculateRawData();
        calculateFIRData();
        calculateKalmanData();
        Isolate();
        decisionMetric();
        /*if(!testdata){
            //createEOGTextFile(fileNum);
            // If we are analysing live data , then we need to document the analysis/saccade report of the new data.. else

            createAnalysisTextFile();
        }*/
        createEOGTextFile(fNum);
    }

    private void decisionMetric()
    {
        double mrt_avg = (mrt_arr[0] + mrt_arr[1] + mrt_arr[2])/3;  //Threshold of 700mSec
        //Total score max = 104, and threshold val is taken as 88
        long total_score = (scr_arr[0] + scr_arr[1] +scr_arr[2] + scr_arr[3] + scr_arr[4] + scr_arr[5] + scr_arr[6]);
        if(total_score>=80 && mrt_avg <=750)
        {
                isAlertGame = true;
        }
        if(avg_sacc_ratio>=.75)
        {
            isAlertEOG = true; //saccade ratio is peakvelocity/saccade duration
        }
        if(isAlertEOG == true && isAlertGame == true)
            alertResult = true;
        else
            alertResult = false;



    }

    public boolean getStatus(){
        return alertResult;
    }

    public Analysis( Bundle val, boolean val2) {
        hasFileNum = false;
        game_data_bundle = val;
        preloaded = val2;         //remove the comments to load the test* data file
        dataObject = DataObject.getInstance();
        samplingRate = dataObject.getEog_samp_rate();
        readFile();
        calculateRawData();
        calculateFIRData();
        calculateKalmanData();
        Isolate();//Major result loading and other functions done in this method.. beware of it!!
        decisionMetric();// Added .. change made on jan 2 2017
        if(!preloaded){
            //createEOGTextFile(fileNum);
            createAnalysisTextFile();
        }

    }



    void readFile()
    {
        if (game_data_bundle != null){
            hasBundle = 2;  //when we have a bundle from game
        }
        switch (hasBundle){        //in case no game bundle.. reads from gamefile
            case 1://----------------Code to Read the game data from txt file and update dataobject----------------------
                // set the arrays to scr_arr, mrt_arr, stim_arr respectively


                //** Variables **//
                ArrayList<Double> temp_list = new ArrayList<Double>();
                long temp_arr[];//,score[],response_time[],Stimuli_times[];


                //********************Code******************//
                try{

                    File mediaStorageDir = new File(Environment.getExternalStorageDirectory(),"GameData");
                    Scanner readData = new Scanner(new FileReader(mediaStorageDir+"/Gamefile.txt"));

                    while(readData.hasNextLine())
                    {
                        String line = readData.nextLine();
                        Scanner scanner = new Scanner(line);
                        scanner.useDelimiter(",");
                        while(scanner.hasNextDouble()){
                            temp_list.add(scanner.nextDouble());
                        }
                        scanner.close();
                    }
                    readData.close();

                }catch(FileNotFoundException e)
                {
                    e.printStackTrace();
                }



                temp_arr = new long[temp_list.size()];
                Iterator <Double> it = temp_list.iterator();

                for(int i = 0;i<temp_list.size();i++)
                {
                    if(it.hasNext())
                    {
                        temp_arr[i]=it.next().longValue();
                    }
                }

                //scr_arr = new long[7];
                System.arraycopy(temp_arr, 0, scr_arr, 0, 7);

                //mrt_arr= new long[7];
                System.arraycopy(temp_arr,7,mrt_arr,0,7 );

                stim_arr= new long[60];
                System.arraycopy(temp_arr, 14, stim_arr, 0, 60);
                //-------
                lastStimulus = stim_arr[59];  //to get the last stimulus time of the VRT test
                firstStimulus = stim_arr[0];

            case 2:    //case when we have a bundle from game
                if (game_data_bundle != null) {
                    bundleHasData = game_data_bundle.getBoolean("HasData");
                }
                if (bundleHasData) {
                    int temp_scr_arr[] = game_data_bundle.getIntArray("score_Final");
                    double temp_mrt_arr[] = game_data_bundle.getDoubleArray("mrt_Final");
                    for (int i=0; i<7 ;i++){
                        scr_arr[i] = (long)temp_scr_arr[i];
                        mrt_arr[i] = (long)temp_mrt_arr[i];
                    }

                    stim_arr = game_data_bundle.getLongArray("stimuli_Final");
                    lastStimulus = stim_arr[59];
                    firstStimulus = stim_arr[0];

                }

                //-----------End of switch case--------------------

                //---------------------

                if(!preloaded) {
                    //lastVRTIndex = (int)(lastStimulus/(1000000000/samplingRate))+samplingRate;        //()/5000000 == ()*200/10^9 ;  200- sampling frequency
                    //firstVRTIndex = (int)(firstStimulus/(1000000000/samplingRate)) - samplingRate;
                    try {
                        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "PWGNGD");
                        String fileNamee = mediaStorageDir+"/myfile_live.txt";
                        FileReader f = new FileReader(fileNamee);    //Testfile1.txt    //esp.txt     //myfile.txt   //Save2.txt
                        //FileReader f = new FileReader("/sdcard/myfile.txt");    //Testfile1.txt    //esp.txt     //myfile.txt   //Save2.txt
                        in = new BufferedReader(f);
                        String text = null;
                        boolean add_to_list = false, continue_reading = true;
                        long adjusted_firstStimulus = firstStimulus - 2000000000;
                        long adjusted_lastStimulus = lastStimulus + 2000000000;

                        while ((text = in.readLine()) != null) {

                            st = new StringTokenizer(text, ",");
                            continue_reading = true;

                            while ((st.hasMoreTokens()) && continue_reading) {

                                String nxt = st.nextToken();
                                if (nxt.matches(time_regex)) {
                                    long trans_time = Long.parseLong(nxt.substring(1));
                                    if (trans_time > adjusted_firstStimulus) {
                                        if (adjusted_firstTransmissionTime == 0) {
                                            adjusted_firstTransmissionTime = trans_time;
                                        }
                                        add_to_list = true;
                                        if (trans_time > adjusted_lastStimulus) {
                                            add_to_list = false;
                                            continue_reading = false;
                                        }
                                    }
                                }
                                if ((nxt.matches(regex)) && add_to_list) {  //  change the OR to AND for correct time matching
                                    list.add(Double.parseDouble(nxt));
                                }
                     /*
                     if(st.nextToken().matches(regex))
                     {
                         list.add(Double.parseDouble(st.nextToken()));
                     }*/
                            }
                        }
                        in.close();

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (hasFileNum) {
                        String fn = "/Test/eog" + fileNum + ".csv";             //
                        readCSVFile(fn);           // change method to check the  game data result(scores) and load the appropriate eog file(eog1-17)
                    } else {
                        readCSVFile("/myfile_live.txt"); // changed from myfile_basic.txt to load previous live file
                    }
                }
        }

    }


    private void readCSVFile(String val) {
        String readText = null;
        BufferedReader br = null;
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "PWGNGD");
        String fileName = mediaStorageDir + val;
        //String fileName = "/sdcard/eog" + val + ".csv";
        try {
            FileReader fr = new FileReader(fileName);
            br = new BufferedReader(fr);
            String line ="";
            while ((line = br.readLine()) != null){
                String[] vals = line.split(",");
                for (String str : vals){
                    if(hasFileNum){
                        list.add(Double.parseDouble(str));
                    } else {
                        if (str.matches(regex)) {
                            list.add(Double.parseDouble(str));
                        }
                    }
                    //readText += str + " , ";
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(br != null){
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private void calculateRawData() {
        arr = new double[list.size()];

        Iterator<Double> iterator = list.iterator();

        for(int i=0;i<list.size();i++) {
            if (iterator.hasNext()) {
                arr[i] = iterator.next();
            }
        }
        series1Numbers = new Number[arr.length];
        double sum_raw =0; double avg_raw =0.0;

        for(int i=0;i<arr.length;i++)
        {
            sum_raw+=arr[i];
        }

        avg_raw = sum_raw/arr.length;

        for(int i=0;i<arr.length;i++)
        {
            series1Numbers[i] = (arr[i]-avg_raw);
        }
        dataObject.setSeries1Numbers(series1Numbers);
    }


    private void calculateKalmanData() {
        series2Numbers = new Number[arr.length];
        Kalman filter = new Kalman();
        filter.extract(series1Numbers);

        double sum_kal =0; double avg_kal =0.0;

        result = new double[series1Numbers.length];

        for (int i = 1; i< series1Numbers.length;i++) //changed i = 1
        {
            result[i] = filter.update(series1Numbers[i].doubleValue());
            sum_kal+=result[i];
            //result[i] = Math.round(result[i]*100.0)/100.0;
        }
        avg_kal = sum_kal/result.length;

        for (int i = 0;i<result.length;i++)
        {
            result[i]=result[i]-avg_kal;
            series2Numbers[i] = result[i];
        }
        dataObject.setSeries2Numbers(series2Numbers);
    }

    private void calculateFIRData() {
        result_FIR = new double[series1Numbers.length];
        FIR filter2 = new FIR();

        double sum_FIR = 0; double avg = 0.0;
        smallest = 0;
        largest=0;
        result= new double[series1Numbers.length];

        for (int i = 0; i< series1Numbers.length;i++)
        {

            result_FIR[i] = filter2.process(series1Numbers[i].doubleValue());
            sum_FIR+= result_FIR[i]; smallest = result[0]; largest = result[0];

            if(result[i] > largest)
                largest = result[i];
            else if (result[i] < smallest)
                smallest = result[i];

        }

        avg = sum_FIR/result_FIR.length;

        series3Numbers = new Number[result_FIR.length];

        for (int i = 0;i<result_FIR.length;i++)
        {
            result_FIR[i] = result_FIR[i]-avg;
            series3Numbers[i] = result_FIR[i];
        }

        dataObject.setSmallest(smallest);
        dataObject.setLargest(largest);
        dataObject.setSeries3Numbers(series3Numbers);
    }



    void Isolate()
    {
        IsolationNew is = new IsolationNew();
        is.extract_Isolate(result_FIR);
        is.calculate_isolation();
        ar_length = is.getLength();

        double maxs[]= new double[ar_length];
        double mins[]= new double[ar_length];

        series_Max_num = new Number [maxs.length];
        series_Min_num = new Number [mins.length];

        maxs = is.getMaxs();
        mins = is.getMins();

        for(int i=0;i<ar_length;i++)
        {
            //maxs[i] = is.getMaxs()[i];
            series_Max_num[i] = maxs[i];

            //mins[i] = is.getMins()[i];
            series_Min_num[i] = mins[i];
        }

        //add to data object class
        dataObject.setIsolated_ar_length(ar_length);
        dataObject.setSeries_Min_num(series_Min_num);
        dataObject.setSeries_Max_num(series_Max_num);

        //--------------------- Dash ------------------------------------
        //saccades = is.getSaccadesArray();
        dataObject.setSaccadesArray(is.getSaccadesArray());     //Array of Saccade objects...
        //saccadeCount = saccades.length;
        System.out.println("-----Number of Saccades found in the graph----:" + saccadeCount);

        if(!preloaded){
            dataObject.setSaccadeCount(calculateSaccadeLatency());
        }


        //***************************************Report For Saccade Data*****************************
        long duration;  //saccade duration of each saccade int the array - saccades
        double peakVelocity, saccRatio;
        int saccade_index = 1;
        long saccade_ltncy = 0;
        String sccd_report = "";
        int validSaccadesCount = 0; //count the number of saccades with nonzero latency
        for (Object sac_obj : dataObject.getSaccadesArray())
        {
            duration = ((SaccadeObject)sac_obj).getSaccadeTime();
            peakVelocity = ((SaccadeObject)sac_obj).getSaccade_peak_velocity();
            saccade_ltncy = ((SaccadeObject)sac_obj).getSaccade_latency();
            saccRatio = ((SaccadeObject)sac_obj).getSaccade_ratio();
            if (saccade_ltncy > 0 | preloaded ) {
                avg_sacc_duration = avg_sacc_duration + duration;
                avg_sacc_peakVelocity = avg_sacc_peakVelocity + peakVelocity;
                avg_sacc_saccade_ltncy = avg_sacc_saccade_ltncy + saccade_ltncy;
                avg_sacc_ratio = avg_sacc_ratio + saccRatio;
                validSaccadesCount++;
            }

            sccd_report = sccd_report + createSaccadeReport(saccade_index, duration, peakVelocity, saccade_ltncy, saccRatio);

            //System.out.println("Saccade Duration of saccade " + saccade_index + " is: " + duration + " milli secs" );
            //System.out.println("Peak velocity of saccade " + saccade_index + " is: " + peakVelocity + " micro volts/sec" );
            saccade_index++;
        }

        if (validSaccadesCount != 0) {
            avg_sacc_duration = avg_sacc_duration / validSaccadesCount;
            avg_sacc_saccade_ltncy = avg_sacc_saccade_ltncy / validSaccadesCount;
            avg_sacc_peakVelocity = avg_sacc_peakVelocity / validSaccadesCount;
            avg_sacc_ratio = avg_sacc_ratio/validSaccadesCount;
        }

        int val_saccCount = dataObject.getSaccadeCount();
        sccd_report = "Number of saccades: "+ val_saccCount+ newline + newline +"AVERAGE VALUES:" + newline + newline+ "Avg. Sacc Ratio: " + avg_sacc_ratio + newline +"Avg. Sacc Duration: " + avg_sacc_duration + " milli Seconds" + newline+ "Avg. Sacc PeakVelocity: " + avg_sacc_peakVelocity + " microVolts/Sec" + newline+ "Avg. Sacc Latency: " + avg_sacc_saccade_ltncy + newline + newline+ sccd_report;

        dataObject.setSaccade_report(sccd_report);
        System.out.println(sccd_report);

        //if (game_data_bundle != null) {
        //**************************************Report For Game Data*********************************
        String mrt_rep = "MRT:: ";
        String scr_rep = "Scores:: ";
        String stim_rep = "Stimuli times:: ";

        for (int k=0; k<7; k++){
            mrt_rep = mrt_rep + mrt_arr[k] + ", ";
            scr_rep = scr_rep + scr_arr[k] + ", ";
        }

        for (int st=0; st<stim_arr.length; st++){
            stim_rep = stim_rep + stim_arr[st] + ", ";
        }

        stim_rep += newline;
        mrt_rep += newline;
        scr_rep += newline;

        dataObject.setMRT_SCR_report(mrt_rep + scr_rep);
        dataObject.setStimuli_times_report(stim_rep);
        //}


        final String saccade_report = sccd_report;
        Thread rep_save_thread = new Thread(new Runnable() {
            @Override
            public void run() {
                BufferedWriter out;
                try
                {
                    File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "GameData");
                    if(!mediaStorageDir.exists())
                    {
                        mediaStorageDir.mkdir();
                    }

                    out = new BufferedWriter(new FileWriter(mediaStorageDir+"/Saccade_Report.txt",true));        //change to true for Appending Textt file.

                    out.write("******************************************************"+"\n");
                    out.write(saccade_report);
                    out.newLine();
                    out.write("******************************************************"+"\n");

                    out.flush();
                    out.close();

                }catch(IOException e)
                {
                    e.printStackTrace();
                }
            }
        });
        rep_save_thread.start();


        //----------------------------------------------------------------
    }

    //Report Generation
    private String createSaccadeReport(int indx, long duration, double peak_vel, long sacc_ltncy, double sacRatio){
        String rep = "";
        rep = rep + "Saccade Number " + indx + " :" + newline + "\t\t" + " Duration " + " : " + duration + " milli Seconds" +newline + "\t\t" +
                " Peak Velocity " + " : " + peak_vel + " microVolts/Sec" + newline + "\t\t" + " Saccade Ratio " + " : "
                + sacRatio + newline  + "\t\t" + " Latency " + " : " + sacc_ltncy + newline + newline;
        return rep;
    }


    private int calculateSaccadeLatency(){
        //long sacc_ltncy_arr[] = new long[dataObject.getSaccadeCount()];
        return mapSaccades2Stimuli();
        //dataObject.setSaccadeLtncyArr(sacc_ltncy_arr);
    }


    private void createAnalysisTextFile() {
        long val = System.currentTimeMillis();

        String fileN = "Analysis "+ val +".txt";
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "PWGNGD/Analysis");
        if(!mediaStorageDir.exists())
        {
            mediaStorageDir.mkdir();
        }

        try {
            out = new BufferedWriter(new FileWriter(mediaStorageDir+"/"+fileN,false));
            out.write(DataObject.getInstance().getSaccade_report());
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    private void createEOGTextFile(int val) {
        String fileN = "Result_eog"+val+".txt";
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "PWGNGD/EOGAnalysis");
        if(!mediaStorageDir.exists())
        {
            mediaStorageDir.mkdir();
        }

        try {
            out = new BufferedWriter(new FileWriter(mediaStorageDir+"/"+fileN,false));
            out.write(DataObject.getInstance().getSaccade_report());
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private int mapSaccades2Stimuli(){
        int count = 0;
        boolean notFound;
        for (Object sac_obj : dataObject.getSaccadesArray())
        {
            int i = 0;
            notFound = true;
            long zeroCrossing = adjusted_firstTransmissionTime + (long)(((SaccadeObject)sac_obj).getSaccade_beginning())*(1000000000/samplingRate);  //  (()/200)*10^9     200 - sampling frequency
            while ((i<(19))&&(notFound)){
                if ((stim_arr[i]<zeroCrossing)&&(stim_arr[i+1]>zeroCrossing)){
                    ((SaccadeObject)sac_obj).setSaccade_latency(zeroCrossing - stim_arr[i]);
                    //sacc_ltncy_arr[arr_indx] = zeroCrossing - stim_arr[i];
                    count++;
                    notFound = false;
                }
                i++;
            }
        }
        return count;
    }
}
