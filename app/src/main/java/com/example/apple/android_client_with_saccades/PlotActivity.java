package com.example.apple.android_client_with_saccades;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.PanZoom;
import com.androidplot.xy.PointLabelFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.StringTokenizer;


public class PlotActivity extends ActionBarActivity  { //implements View.onTouchListener
    private XYPlot plotter;
    double arr[];
    ArrayList<Double> list = new ArrayList<Double>();
    private PointF minXY;
    private PointF maxXY;
    XYSeries series1, series2, series3, series4, series5;
    double smallest, largest;
    Number series1Numbers[], series2Numbers[], series3Numbers[], series_Max_num[], series_Min_num[];
    Analysis a;
    int saccadeCount = 0, arrayLength = 0;

    private DataObject dataObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.simple_xy_plot_example);

        plotter = (XYPlot) findViewById(R.id.plot);


        dataObject = DataObject.getInstance();
        series1Numbers = dataObject.getSeries1Numbers();
        series2Numbers = dataObject.getSeries2Numbers();
        series3Numbers = dataObject.getSeries3Numbers();
        arrayLength = dataObject.getRawArr_length();

        int isolateLength = dataObject.getIsolated_ar_length();
        series_Max_num = dataObject.getSeries_Max_num();
        series_Min_num = dataObject.getSeries_Min_num();
        smallest = dataObject.getSmallest();
        largest = dataObject.getLargest();

        final Number[] domainLabels =series1Numbers;
        /******************************************************************************************************************
         Plotting of Analyzed data
         *******************************************************************************************************************/


        // Turn the above arrays into XYSeries':
        XYSeries series1 = new SimpleXYSeries(
                Arrays.asList(series1Numbers),          // SimpleXYSeries takes a List so turn our array into a List
                SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, // Y_VALS_ONLY means use the element index as the x value
                " RAW");  // Set the display title of the series

        XYSeries series2 = new SimpleXYSeries(Arrays.asList(series2Numbers), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "  KAL");
        XYSeries series3 = new SimpleXYSeries(Arrays.asList(series3Numbers), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "  MED");//FIR
        XYSeries series4 = new SimpleXYSeries(Arrays.asList(series_Max_num), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "  Max");
        XYSeries series5 = new SimpleXYSeries(Arrays.asList(series_Min_num), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "  Min");
        // Create a formatter to use for drawing a series using LineAndPointRenderer
        // and configure it from xml:

        LineAndPointFormatter series1Format = new LineAndPointFormatter(Color.rgb(0, 250, 0), null /* Color.rgb(0,250,0)*/, null, null);
        series1Format.configure(getApplicationContext(), R.xml.line_point_formatter_with_plf1);

        plotter.calculateMinMaxVals();
        // minXY = new PointF(plot.getCalculatedMinX().floatValue(), plot.getCalculatedMinY().floatValue());
        // maxXY = new PointF(plot.getCalculatedMaxX().floatValue(), plot.getCalculatedMaxY().floatValue());

        plotter.addSeries(series1, series1Format);

        LineAndPointFormatter series2Format = new LineAndPointFormatter(Color.rgb(255, 250, 0), null, null, null);
        series1Format.configure(getApplicationContext(), R.xml.line_point_formatter_with_plf2);

       /* plotter.addSeries(series2, series2Format); //new LineAndPointFormatter(Color.rgb(0, 250, 0), Color.rgb(0, 250, 0), null, (PointLabelFormatter) null));
            series2 removed from plotting
        */
        LineAndPointFormatter series3Format = new LineAndPointFormatter(Color.rgb(220, 10, 10), null, null, null);
        series1Format.configure(getApplicationContext(), R.xml.line_point_formatter_with_plf3);

        plotter.addSeries(series3, series3Format);

        plotter.addSeries(series4, new LineAndPointFormatter(null/*Color.rgb(255,250,0)*/, Color.rgb(0, 25, 200), null, (PointLabelFormatter) null));
        plotter.addSeries(series5, new LineAndPointFormatter(null/* Color.rgb(220, 10, 10)*/, Color.rgb(0, 200, 210), null, (PointLabelFormatter) null));

        Paint p = new Paint();
        p.setColor(Color.WHITE);

        plotter.setDomainLabel("Data points");
        plotter.setRangeLabel("Micro-volts");

        plotter.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).setFormat(new Format() {
            @Override
            public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
                int i = Math.round(((Number) obj).floatValue());
                return toAppendTo.append(domainLabels[i]);
            }
            @Override
            public Object parseObject(String source, ParsePosition pos) {
                return null;
            }
        });

        plotter.getGraph().getBackgroundPaint().setColor(Color.BLACK);

        plotter.getGraph().setDomainSubGridLinePaint(p); // All domain lines are made black
        plotter.getGraph().setDomainGridLinePaint(p);    //- do- for grid

        plotter.getGraph().setRangeSubGridLinePaint(p); // all grid lines are made black
        plotter.getGraph().setRangeGridLinePaint(p);

        plotter.getBackgroundPaint().setColor(Color.BLACK);


        plotter.getGraph().getGridBackgroundPaint().setColor(Color.BLACK); //For grid color

       // plotter.setRangeBoundaries(smallest - 100, largest + 100, BoundaryMode.FIXED);

        plotter.setDomainLabel("Data points");
        plotter.setRangeLabel("Micro-volts");

        plotter.getGraph().setLinesPerDomainLabel(2);
        plotter.getGraph().setLinesPerRangeLabel(3);

        // plot.setTicksPerRangeLabel(3);
        // plot.setTicksPerDomainLabel(2);

       // plot.setOnTouchListener(this); // comment added now

        plotter.setDomainBoundaries(1, 20, BoundaryMode.AUTO);
        plotter.setRangeBoundaries(0,0, BoundaryMode.AUTO);

        PanZoom.attach(plotter);
       // plotter.getOuterLimits().set(0, 100, 0, 100);

    }
}

   